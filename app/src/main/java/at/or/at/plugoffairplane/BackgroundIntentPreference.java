package at.or.at.plugoffairplane;

import android.content.Context;
import android.preference.Preference;

public class BackgroundIntentPreference extends Preference implements Preference.OnPreferenceClickListener {

    BackgroundIntentPreference(Context context) {
        super(context);
        this.setOnPreferenceClickListener(this);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        BackgroundIntentService.requestRoot(getContext());
        return true;
    }
}