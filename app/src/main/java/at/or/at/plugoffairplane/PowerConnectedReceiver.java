package at.or.at.plugoffairplane;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

public class PowerConnectedReceiver extends BroadcastReceiver {

    public static final String TAG = "PowerConnectedReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive " + intent);
        if (intent == null || !Intent.ACTION_POWER_CONNECTED.equals(intent.getAction())) {
            return;
        }

        if (isAirplaneModeOn(context)) {
            Log.i(TAG, "isAirplaneModeOn " + isAirplaneModeOn(context));
            BackgroundIntentService.turnOffAirplaneMode(context);
        }
    }

    public static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }
}
