package at.or.at.plugoffairplane;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import eu.chainfire.libsuperuser.Shell;

public class BackgroundIntentService extends IntentService {

    private static final String ACTION_TURN_OFF_AIRPLANE_MODE =
            "at.or.at.plugoffairplane.action.TURN_OFF_AIRPLANE_MODE";
    private static final String ACTION_REQUEST_ROOT =
            "at.or.at.plugoffairplane.action.REQUEST_ROOT";

    public static void turnOffAirplaneMode(Context context) {
        sendIntent(context, ACTION_TURN_OFF_AIRPLANE_MODE, null);
    }

    public static void requestRoot(Context context) {
        sendIntent(context, ACTION_REQUEST_ROOT, null);
    }

    public static void sendIntent(Context context, String action, String text) {
        if (context == null) {
            return;
        }

        Intent intent = new Intent(context, BackgroundIntentService.class);
        intent.setAction(action);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        context.startService(intent);
    }

    public BackgroundIntentService() {
        super("BackgroundIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return;
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (telephony != null && telephony.isNetworkRoaming() && !prefs.getBoolean("pref_enabled_on_roaming", true)) {
            Toast.makeText(this, R.string.roaming_toast_msg, Toast.LENGTH_LONG).show();
        }
        String action = intent.getAction();
        if (ACTION_TURN_OFF_AIRPLANE_MODE.equals(action)) {
            if (prefs.getBoolean("pref_enabled", true)) {
                Shell.SU.run("settings put global " + Settings.Global.AIRPLANE_MODE_ON + " 0");
                Shell.SU.run("am broadcast -a " + Intent.ACTION_AIRPLANE_MODE_CHANGED + " --ez state false");
            }
        } else if (ACTION_REQUEST_ROOT.equals(action)) {
            Shell.SU.available();
        } else if (Settings.Global.AIRPLANE_MODE_RADIOS.equals(action)) {
            Shell.SU.run("settings put global " + Settings.Global.AIRPLANE_MODE_RADIOS
                    + " " + intent.getStringExtra(Intent.EXTRA_TEXT));
        } else if (SettingsActivity.AIRPLANE_MODE_TOGGLEABLE_RADIOS.equals(action)) {
            Shell.SU.run("settings put global " + SettingsActivity.AIRPLANE_MODE_TOGGLEABLE_RADIOS
                    + " " + intent.getStringExtra(Intent.EXTRA_TEXT));
        } else {
            throw new IllegalArgumentException("Unsupported action: " + intent);
        }
    }
}