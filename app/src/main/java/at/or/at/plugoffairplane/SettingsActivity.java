package at.or.at.plugoffairplane;

import android.app.Activity;
import android.content.ContentResolver;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.text.TextUtils;
import eu.chainfire.libsuperuser.Shell;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;

public class SettingsActivity extends Activity {
    public static final String TAG = "SettingsActivity";

    /**
     * This is {@code Settings.Global.AIRPLANE_MODE_TOGGLEABLE_RADIOS}, but its
     * hidden in the Android SDK.
     */
    public static final String AIRPLANE_MODE_TOGGLEABLE_RADIOS;

    /**
     * This is {@code Settings.Global.RADIO_WIMAX}, but its hidden in the Android SDK.
     */
    public static final String RADIO_WIMAX;

    /**
     * In case some Android implementer does something weird with these values;
     */
    static {
        String airplaneModeToggleableRadios = "airplane_mode_toggleable_radios";
        String radioWimax = "wimax";
        try {
            Field field;
            field = Settings.Global.class.getDeclaredField("AIRPLANE_MODE_TOGGLEABLE_RADIOS");
            field.setAccessible(true);
            airplaneModeToggleableRadios = (String) field.get(field);
            field = Settings.Global.class.getDeclaredField("RADIO_WIMAX");
            field.setAccessible(true);
            radioWimax = (String) field.get(field);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        AIRPLANE_MODE_TOGGLEABLE_RADIOS = airplaneModeToggleableRadios;
        RADIO_WIMAX = radioWimax;
    }

    public static class MySettingsFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
        }

        @Override
        public void onResume() {
            super.onResume();

            if (!Shell.SU.available()) {
                PreferenceScreen preferenceScreen = getPreferenceScreen();
                preferenceScreen.removeAll();
                BackgroundIntentPreference pref = new BackgroundIntentPreference(getActivity());
                pref.setTitle(R.string.no_root_title);
                pref.setSummary(R.string.no_root_summary);
                preferenceScreen.addPreference(pref);
                return;
            }

            ContentResolver contentResolver = getActivity().getContentResolver();
            for (String key : new String[]{Settings.Global.AIRPLANE_MODE_RADIOS, AIRPLANE_MODE_TOGGLEABLE_RADIOS}) {
                String radios = Settings.Global.getString(contentResolver, key);
                if (TextUtils.isEmpty(radios)) {
                    continue;
                }
                for (String radio : radios.split(",")) {
                    CheckBoxPreference pref = (CheckBoxPreference) findPreference(key + "_" + radio);
                    pref.setOnPreferenceChangeListener(ON_PREFERENCE_CHANGE_LISTENER);
                    pref.setDefaultValue(true);
                    pref.setChecked(true);
                }
            }
        }

        private final Preference.OnPreferenceChangeListener
                ON_PREFERENCE_CHANGE_LISTENER = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object checked) {
                String key = preference.getKey();
                String groupKey;
                if (key.startsWith(Settings.Global.AIRPLANE_MODE_RADIOS)) {
                    groupKey = Settings.Global.AIRPLANE_MODE_RADIOS;
                } else if (key.startsWith(AIRPLANE_MODE_TOGGLEABLE_RADIOS)) {
                    groupKey = AIRPLANE_MODE_TOGGLEABLE_RADIOS;
                } else {
                    throw new IllegalStateException("Unknown preference changed: " + preference);
                }
                ArrayList<String> state = new ArrayList<>();
                for (String radio : RADIO_NAMES) {
                    String currentKey = groupKey + "_" + radio;
                    CheckBoxPreference pref = (CheckBoxPreference) MySettingsFragment.this.findPreference(currentKey);
                    if (key.equals(currentKey)) {
                        if ((boolean) checked) {
                            state.add(radio);
                        }
                    } else if (pref.isChecked()) {
                        state.add(radio);
                    }
                }
                Collections.sort(state);
                BackgroundIntentService.sendIntent(getActivity(), groupKey, TextUtils.join(",", state));
                return true;
            }
        };
    }

    private static final String[] RADIO_NAMES = {
            Settings.Global.RADIO_BLUETOOTH,
            Settings.Global.RADIO_CELL,
            Settings.Global.RADIO_NFC,
            Settings.Global.RADIO_WIFI,
            RADIO_WIMAX,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new MySettingsFragment())
                .commit();
    }
}
