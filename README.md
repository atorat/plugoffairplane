
# Plug Off Airplane

This is a very simple app that turns off Airplane Mode when the USB
cable is plugged in.  The idea is to make sure Airplane Mode is turned
off at least once a day, in a safe location in terms of metadata: the
home.  Your telecom already knows your device belongs to its billing
address.  This makes it easier to use Airplane Mode to disable the
cellular radio during the day, when it is not needed.

This also includes preferences for enabling which radios Airplane Mode
actually controls.  For example, Wi-Fi can be removed from Airplane
Mode, so that Wi-Fi does not get shutoff when Airplane Mode is
enabled.

Since this app requires root to work, the code is meant to be as small
and simple as possible.  It also only includes a single library:
libsuperuser.  The keeps the code easily auditable.

<a href="https://f-droid.org/packages/at.or.at.plugoffairplane" target="_blank">
<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80"/></a>


## Nightly builds

Each time commits are pushed, a nightly build is added to this F-Droid repo:

[![https://gitlab.com/atorat/plugoffairplane-nightly/raw/master/fdroid/repo](https://gitlab.com/atorat/plugoffairplane-nightly/raw/master/icon.png)](https://gitlab.com/atorat/plugoffairplane-nightly/raw/master/fdroid/repo)


## Translation

This is not currently hooked up to and translation service.
Translations submitted via merge request are most welcome!
